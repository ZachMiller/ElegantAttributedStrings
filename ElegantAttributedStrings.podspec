Pod::Spec.new do |s|
  s.name             = 'ElegantAttributedStrings'
  s.version          = '1.0.1'
  s.summary          = 'Easy to use extension for creating attributed strings.'
 
  s.description      = <<-DESC
This project provides an easy to use extension for creating attributed strings. Instead of searching the Apple documentation for the relevant keys and values for each attribute, this project provides a ready-to-use enum with properly typed associated values.
                       DESC
 
  s.homepage         = 'https://gitlab.com/ZachMiller/ElegantAttributedStrings'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Zach Miller' => 'zach@200apps.co' }
  s.source           = { :git => 'https://gitlab.com/ZachMiller/ElegantAttributedStrings.git', :tag => s.version.to_s }
 
  s.ios.deployment_target = "9.0"
  s.source_files = 'ElegantAttributedStrings/Main_Files/*'
 
end