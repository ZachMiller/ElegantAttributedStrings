# ElegantAttributedStrings

[![Version](https://img.shields.io/cocoapods/v/ElegantAttributedStrings.svg?style=flat)](https://cocoapods.org/pods/ElegantAttributedStrings)
[![License](https://img.shields.io/cocoapods/l/ElegantAttributedStrings.svg?style=flat)](https://cocoapods.org/pods/ElegantAttributedStrings)
[![Platform](https://img.shields.io/cocoapods/p/ElegantAttributedStrings.svg?style=flat)](https://cocoapods.org/pods/ElegantAttributedStrings)

<img width=300 height=200 src="ScreenShot.png" alt="Screenshot"/>


## Usage

```swift
import UIKit
import ElegantAttributedStrings

class ViewController: UIViewController {

    @IBOutlet var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let boldFont = UIFont.boldSystemFont(ofSize: 15)
        let regularFont = UIFont.systemFont(ofSize: 15)
        let italicsFont = UIFont.italicSystemFont(ofSize: 15)
        
        let formattedString = NSMutableAttributedString()
        
        formattedString
            .append("Hello ", with: .font(boldFont), .foregroundColor(.blue))
            .append("Goodbye ", with: .font(regularFont), .foregroundColor(.red))
            .append("OK ", with: .font(italicsFont), .foregroundColor(.green))
            .append("Yo ", with: .backgroundColor(.yellow))
            .append("What", with: .underlineStyle(1))
            .append(" ")
            .append("Enough", with: .strikethroughStyle(1))
        
        self.label.attributedText = formattedString
    }
}
```


## Requirements

* iOS 9.0+

## Installation

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate ElegantAttributedStrings into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
use_frameworks!

pod 'ElegantAttributedStrings'
```

Then, run the following command:

```bash
$ pod install
```


## Author

Zach Miller, zach@200apps.co


## License

ElegantAttributedStrings is available under the MIT license. See the LICENSE file for more info.