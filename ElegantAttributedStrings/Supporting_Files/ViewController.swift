//
//  ViewController.swift
//  ElegantAttributedStrings
//
//  Created by Zach Miller on 6/13/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let boldFont = UIFont.boldSystemFont(ofSize: 15)
        let regularFont = UIFont.systemFont(ofSize: 15)
        let italicsFont = UIFont.italicSystemFont(ofSize: 15)
        
        let formattedString = NSMutableAttributedString()
        let shouldAppendYo = false
        
        formattedString
            .append("Hello ", with: .font(boldFont), .foregroundColor(.blue))
            .append("Goodbye ", with: .font(regularFont), .foregroundColor(.red))
            .append("OK ", with: .font(italicsFont), .foregroundColor(.green))
            .append("Yo ", with: .backgroundColor(.yellow), if: shouldAppendYo)
            .append("What", with: .underlineStyle(1))
            .append(" ")
            .append("Enough", with: .strikethroughStyle(1))
        
        self.label.attributedText = formattedString
    }
}
