//
//  NSAttributedStringAttribute.swift
//  ElegantAttributedStrings
//
//  Created by Zach Miller on 6/13/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import Foundation
import UIKit

public enum NSAttributedStringAttribute {
    case attachment(NSTextAttachment)
    case backgroundColor(UIColor)
    case baselineOffset(NSNumber)
    case expansion(NSNumber)
    case font(UIFont)
    case foregroundColor(UIColor)
    case kern(NSNumber)
    case ligature(NSNumber)
    case link(NSURL)
    case obliqueness(NSNumber)
    case paragraphStyle(NSParagraphStyle)
    case shadow(NSShadow)
    case strikethroughColor(UIColor)
    case strikethroughStyle(NSNumber)
    case strokeColor(UIColor)
    case strokeWidth(NSNumber)
    case textEffect(NSString)
    case underlineColor(UIColor)
    case underlineStyle(NSNumber)
    case verticalGlyphForm(NSNumber)
    case writingDirection(NSArray)
}

fileprivate extension NSAttributedStringAttribute {
    static func keyValuePair(from attribute: NSAttributedStringAttribute) -> (NSAttributedStringKey, Any) {
        switch attribute {
        case .attachment(let value): return (NSAttributedStringKey.attachment, value)
        case .backgroundColor(let value): return (NSAttributedStringKey.backgroundColor, value)
        case .baselineOffset(let value): return (NSAttributedStringKey.baselineOffset, value)
        case .expansion(let value): return (NSAttributedStringKey.expansion, value)
        case .font(let value): return (NSAttributedStringKey.font, value)
        case .foregroundColor(let value): return (NSAttributedStringKey.foregroundColor, value)
        case .kern(let value): return (NSAttributedStringKey.kern, value)
        case .ligature(let value): return (NSAttributedStringKey.ligature, value)
        case .link(let value): return (NSAttributedStringKey.link, value)
        case .obliqueness(let value): return (NSAttributedStringKey.obliqueness, value)
        case .paragraphStyle(let value): return (NSAttributedStringKey.paragraphStyle, value)
        case .shadow(let value): return (NSAttributedStringKey.shadow, value)
        case .strikethroughColor(let value): return (NSAttributedStringKey.strikethroughColor, value)
        case .strikethroughStyle(let value): return (NSAttributedStringKey.strikethroughStyle, value)
        case .strokeColor(let value): return (NSAttributedStringKey.strokeColor, value)
        case .strokeWidth(let value): return (NSAttributedStringKey.strokeWidth, value)
        case .textEffect(let value): return (NSAttributedStringKey.textEffect, value)
        case .underlineColor(let value): return (NSAttributedStringKey.underlineColor, value)
        case .underlineStyle(let value): return (NSAttributedStringKey.underlineStyle, value)
        case .verticalGlyphForm(let value): return (NSAttributedStringKey.verticalGlyphForm, value)
        case .writingDirection(let value): return (NSAttributedStringKey.writingDirection, value)
        }
    }
}

public extension NSMutableAttributedString {
    @discardableResult func append(_ string: String?, with attributes: NSAttributedStringAttribute..., if shouldAppend: Bool = true) -> NSMutableAttributedString {
        guard let string = string, shouldAppend else { return self }
        let attributeKeyValuePairs = attributes.map(NSAttributedStringAttribute.keyValuePair)
        let attributesDictionary = Dictionary(uniqueKeysWithValues: attributeKeyValuePairs)
        let nextAttributedString = NSMutableAttributedString(string: string, attributes: attributesDictionary)
        self.append(nextAttributedString)
        return self
    }
}
